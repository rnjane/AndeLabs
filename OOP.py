class Animal:
    #data hiding(private data members)
    __name = ""
    __sound = ""
    __weight = 0
    __height = 0

    def __init__(self, name, sound, weight, height):
        self.__name = name
        self.__sound = sound
        self.__weight = weight
        self.__height = height

    #Encapsulation - achieved by using getters and setters
    def setName(self, name):
        self.__name = name

    def getName(self):
        return self.__name

    def setSound(self, sound):
        self.__sound = sound

    def getSound(self):
        return self.__sound

    def setHeight(self, height):
        self.__height = height

    def getHeight(self):
        return self.__height

    def setWeight(self, weight):
        self.__weight = weight

    def getWeight(self):
        return self.__weight

    def gettype(self):
        print("Animal")

merinho = Animal("Sheep", "Meee", 12, 25)

#inheritance Dog class inheriting from animal class
class Dog(Animal):
    __owner = ""

    def __init__(self, owner, name, sound, weight, height):
        self.__owner = owner
        super(Dog, self).__init__(name, sound, weight, height)

    def gettype(self):
            print("Dog")

    #method overloading
    def multipleSounds(self, howMany = None):
        if howMany is None:
            print(self.getSound())
        else:
            print(self.getSound() * howMany)


simba = Dog("Roba", "Simba", "Bark", 50, 250)


class AnimalType:
    def gettype(self, Animal):
        Animal.gettype()

animal1 = AnimalType()
#polymorphism here
animal1.gettype(merinho)
animal1.gettype(simba)
