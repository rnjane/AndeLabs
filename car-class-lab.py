class Car:
    __name__ = "General"
    __cartype = ""
    __model = "GM"
    __make = ""

    def __init__(self, cartype, model, make, name):
        self.__type = cartype
        self.__model = model
        self.__make = make
        self.__name = name

    def settype(self, cartype):
        self.__cartype = cartype

    def getcartype(self):
        return self.__cartype

    def setmodel(self, model):
        self.__model = model

    def getmodel(self):
        return self.__model

    def setmake(self, make):
        self.__make = make

    def getmake(self):
        return self.__make

    def toString(self):
        return "Type of the car is {}, model is {} and the make is {}".format(self.__type,
                                                                              self.__model,
                                                                              self.__make)
S_Class = Car("Sedan","S Class","Mercedes")

print(S_Class.toString())

